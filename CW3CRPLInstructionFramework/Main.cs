﻿using Mono.Cecil;
using MonoMod;
using System;
using System.Collections.Generic;
using System.Text;
using Mono.Cecil.Cil;
using System.Linq;

public static class CRPLInstructionFramework
{
    public struct CustomInstructionDefinition
    {
        public Action<CrplCore> callback;
    }

    public static Dictionary<string, CustomInstructionDefinition> instructions = new Dictionary<string, CustomInstructionDefinition>();
}

public class patch_CrplCore : CrplCore
{
    [MonoModIgnore]
    public patch_CrplCore(UnitManager baseUnit, string scriptName) : base(baseUnit, scriptName) { }

    [CustomAttribs.CustomPatch1]
    private void __HandleDefaultStatement(Command command)
    {
        Console.WriteLine("Test");
    }
}

public class patch_CrplCompiler : CrplCompiler
{
    [MonoModIgnore]
    private List<CrplCore.Command> commands;

    [CustomAttribs.CustomPatch2]
    private bool __HandleCustomStatement(Token token)
    {
        return false;
    }

    [MonoModIgnore]
    private extern CrplCore.Command AppendCommand(CrplCore.STATEMENT statement, string arg, int lineNumber);

    private CrplCore.Command AppendCustomCommand(CrplCore.STATEMENT statement, int lineNumber)
    {
        CrplCore.Command command = new CrplCore.Command(statement, lineNumber);
        commands.Add(command);
        return command;
    }
    
    [MonoModIgnore]
    private struct Token
    {
        public string tok;

        public int lineNumber;

        public Token(string tok, int lineNumber)
        {
            this.tok = tok;
            this.lineNumber = lineNumber;
        }
    }
}