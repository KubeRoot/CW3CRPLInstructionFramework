﻿using Mono.Cecil;
using MonoMod;
using System;
using System.Collections.Generic;
using System.Text;
using Mono.Cecil.Cil;
using System.Linq;

namespace CustomAttribs
{
    [MonoModCustomMethodAttribute("CustomPatch1")] // MonoMod.MonoModRules::CustomPatch1
    public class CustomPatch1 : Attribute
    {
        public CustomPatch1() { }
    }

    [MonoModCustomMethodAttribute("CustomPatch2")] // MonoMod.MonoModRules::CustomPatch2
    public class CustomPatch2 : Attribute
    {
        public CustomPatch2() { }
    }
}

namespace MonoMod
{
    static class MonoModRules
    {
        public static void CustomPatch1(MethodDefinition patch_method, CustomAttribute attrib)
        {
            MonoModder modder = InlineRT.MMILRT.Modder;
            ModuleDefinition module = modder.Module;

            TypeDefinition crplcore = module.GetType("CrplCore");
            MethodDefinition target_method = crplcore.FindMethod("ProcessCommands");

            if (!target_method.HasBody)
            {
                throw new Exception("CrplCore.ProcessCommands has no body");
            }

            var processor = target_method.Body.GetILProcessor();
            var instructions = target_method.Body.Instructions.ToList();

            foreach (var instr in instructions)
            {
                if (instr.OpCode == OpCodes.Switch)
                {
                    var next = instr.Next;
                    if (next.OpCode == OpCodes.Br)
                    {
                        var var_index = instr.Previous.Previous.Previous.Previous.Operand as VariableDefinition;
                        var orig_target = next.Operand as Instruction;
                        var new_target = processor.Create(OpCodes.Ldarg_0);
                        processor.Append(new_target);
                        processor.Emit(OpCodes.Ldloc_S, var_index);
                        processor.Emit(OpCodes.Call, patch_method);
                        processor.Emit(OpCodes.Br, orig_target);
                        next.Operand = new_target;
                    }
                }
            }
        }

        public static void CustomPatch2(MethodDefinition patch_method, CustomAttribute attrib)
        {
            MonoModder modder = InlineRT.MMILRT.Modder;
            ModuleDefinition module = modder.Module;

            TypeDefinition crplcompiler = module.GetType("CrplCompiler");
            MethodDefinition target_method = crplcompiler.FindMethod("Compile");

            if (!target_method.HasBody || !target_method.Body.HasExceptionHandlers)
            {
                throw new Exception("CrplCompiler.Compile has no body");
            }

            var processor = target_method.Body.GetILProcessor();
            var instructions = target_method.Body.Instructions;
            var searchfield = module.GetType("CrplCompiler/Token")?.FindField("lineNumber");
            if (searchfield == null)
            {
                throw new Exception("Couldn't find CrplCompiler.Token");
            }

            var searchname = searchfield.FullName;

            var handlers = target_method.Body.ExceptionHandlers;
            foreach (var handler in handlers)
            {
                if (handler.HandlerType == ExceptionHandlerType.Catch)
                {
                    var start = handler.HandlerStart.Next;
                    var end = handler.HandlerEnd;

                    bool success = false;

                    var it = start;
                    while (it != end)
                    {
                        if (it.OpCode == OpCodes.Ldstr && (it.Operand as string) == ": Invalid Token: ")
                        {
                            success = true;
                            break;
                        }
                        it = it.Next;
                    }

                    if (!success)
                    {
                        continue;
                    }

                    it = start;
                    VariableDefinition variable = null;
                    while (it != end)
                    {
                        if (it.OpCode == OpCodes.Ldfld && (it.Operand as FieldReference).FullName == searchname)
                        {
                            variable = it.Previous.Operand as VariableDefinition;
                            break;
                        }
                        it = it.Next;
                    }

                    if (variable == null)
                    {
                        throw new Exception("Couldn't find token local variable");
                    }

                    processor.InsertBefore(start, processor.Create(OpCodes.Ldarg_0));
                    processor.InsertBefore(start, processor.Create(OpCodes.Ldloc, variable));
                    processor.InsertBefore(start, processor.Create(OpCodes.Call, patch_method));
                    processor.InsertBefore(start, processor.Create(OpCodes.Brfalse, start));
                    processor.InsertBefore(start, processor.Create(OpCodes.Leave, end));
                }
            }
        }
    }
}
